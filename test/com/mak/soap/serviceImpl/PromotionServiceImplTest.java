package com.mak.soap.serviceImpl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.mak.soap.conversion.JAXBConvert;
import com.mak.soap.entity.Deal;

public class PromotionServiceImplTest {
    private JAXBConvert jaxbConvert;
    private List<Deal> dealsList;
    
	@Before
	public void setUp() throws Exception {
		jaxbConvert = new JAXBConvert();
		dealsList = jaxbConvert.convertJAXBToJava();
	}

	@Test
	public void testGetAllDealsSize() {
		List<Deal> dealsList = jaxbConvert.convertJAXBToJava();
		int expected = 5;
		int actual = dealsList.size();
		assertEquals(expected,actual);
	}
	
	@Test
	public void testDealNameXML()
	{
		String actual=dealsList.get(0).getName();
		
		String expected= "T-Mobile+Netflix";
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testDealDescriptionXML()
	{
		String actual=dealsList.get(0).getDescription();
		
		String expected= "T-Mobile ONE� unlimited family plans now include Netflix on us.";
		assertEquals(expected, actual);
		
	}

}
