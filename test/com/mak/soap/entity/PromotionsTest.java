package com.mak.soap.entity;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PromotionsTest {

	private Promotions promo;
	private List<Deal> dealsList = new ArrayList<>();

	@Before
	public void setUp() throws Exception {
		promo = new Promotions();
		dealsList.add(new Deal("offer1", "aaaa"));
		promo.setDeal(dealsList);
	}

	@Test
	public void testGetDeal() {
		String actual = promo.getDeal().get(0).getName();
		String expected = "offer1";

		assertEquals(expected, actual);
	}

}
