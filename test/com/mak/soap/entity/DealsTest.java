package com.mak.soap.entity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DealsTest {

	private Deal deal;

	@Before
	public void setUp() throws Exception {

		deal = new Deal("ALCATEL LINKZONE�", "Stay connected with the lightweight");
	}

	// TODO GetDescription Test

	@Test
	public void getDescriptionTest() {

		String actual = deal.getDescription();

		String expected = "Stay connected with the lightweight";

		assertEquals(expected, actual);

	}
	// TODO GetName Test

	@Test
	public void getNameTest() {

		String actual = deal.getName();

		String expected = "ALCATEL LINKZONE�";

		assertEquals(expected, actual);

	}

	@Test
	public void toStringTest() {
		String actual = deal.toString();
		String expected = "Offer [name=ALCATEL LINKZONE�, description=Stay connected with the lightweight]";

		assertEquals(expected, actual);
	}

}
