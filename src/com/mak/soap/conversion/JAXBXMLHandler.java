package com.mak.soap.conversion;

import java.net.URL;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.mak.soap.entity.Deal;
import com.mak.soap.entity.Promotions;

public class JAXBXMLHandler {

	public static List<Deal> unmarshal1(URL url) throws JAXBException {

		Promotions promotions = new Promotions();
		JAXBContext context = JAXBContext.newInstance(Promotions.class);
		Unmarshaller unmarshall = context.createUnmarshaller();
		promotions=(Promotions) unmarshall.unmarshal(url);
		return promotions.getDeal();
	}

}
