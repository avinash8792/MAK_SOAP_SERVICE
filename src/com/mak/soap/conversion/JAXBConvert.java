package com.mak.soap.conversion;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.mak.soap.entity.Deal;

public class JAXBConvert {

	public List<Deal> convertJAXBToJava(){
		List<Deal> unmarshalledList = new ArrayList<>();
		
		try {
			unmarshalledList = JAXBXMLHandler.unmarshal1(new URL("https://0d89b5b5-5ccb-49ed-87cd-c784d30e534d.mock.pstmn.io"));
			
		} catch (IOException | JAXBException e) {
			
			e.printStackTrace();
		}
		return unmarshalledList;
		
	}
}
