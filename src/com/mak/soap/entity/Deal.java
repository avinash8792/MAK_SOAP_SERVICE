package com.mak.soap.entity;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {"name","description"})
public class Deal {

	private String name;
	private String description;
	
	public Deal (){
		
	}
	
	
	
	public Deal(String name, String description) {
		
		this.name = name;
		this.description = description;
	}



	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return "Offer [name=" + name + ", description=" + description + "]";
	}
	
	
	
	
}
