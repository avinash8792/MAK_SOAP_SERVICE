package com.mak.soap.service;

import java.util.List;

import com.mak.soap.entity.Deal;

public interface PromotionsService {
	
	public List<Deal> getAllDeals();

}
