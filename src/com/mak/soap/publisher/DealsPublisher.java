package com.mak.soap.publisher;

import javax.xml.ws.Endpoint;

import com.mak.soap.serviceImpl.PromotionServiceImpl;

public class DealsPublisher {
	
	public static void main(String[] args) {
		
		Endpoint.publish("http://localhost:8081/promotion/deal", new PromotionServiceImpl());
	}

}
